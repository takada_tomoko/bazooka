﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision collision)
    {
        // 弾が地面に当たった時の判定
        if (collision.collider.name == "Plane")
        {
            // 2.5秒後に弾を消去する
            Destroy(this.gameObject, 2.5f);
        }
        // 弾がクマに当たった時の判定
        else if (collision.collider.tag == "Teddy")
        {
            // 瞬時に弾を消去する
            Destroy(this.gameObject);
        }
    }
}

