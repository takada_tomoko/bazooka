﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private static SoundManager instance;
    private AudioSource audioSouce;

    // スタート時
    public AudioClip startVoiceAudio;
    // バズーカを撃った時
    public AudioClip fireVoice1Audio;
    public AudioClip fireVoice2Audio;
    // 終了時
    public AudioClip endVoiceAudio;
    // 音声ファイルの参照
    private AudioSource voiceAudioSource;
    // 爆発時
    public AudioClip explosionAudio;

    void Start()
    {
        // ゲーム開始時に音声を再生
        this.voiceAudioSource = gameObject.AddComponent<AudioSource>();
    }

    // バズーカを撃った時の掛け声
    public void PlayStartVoice()
    {
        voiceAudioSource.PlayOneShot(startVoiceAudio);
    }
    public void PlayFireVoice1()
    {
        voiceAudioSource.PlayOneShot(fireVoice1Audio);
    }
    public void PlayFireVoice2()
    {
        voiceAudioSource.PlayOneShot(fireVoice2Audio);
    }

    // 終了時のセリフ
    public void PlayEndVoice()
    {
        voiceAudioSource.PlayOneShot(endVoiceAudio);
    }

    public static SoundManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = (SoundManager)FindObjectOfType(typeof(SoundManager));
                if (instance == null)
                {
                    Debug.LogError("SoundManager Instance Error");
                }
            }
            return instance;
        }
    }

    /// <summary>
    /// 爆発音発生メソッド
    /// </summary>
    public void PlayExplosionAudio()
    {
        audioSouce.PlayOneShot(explosionAudio);
    }
}
