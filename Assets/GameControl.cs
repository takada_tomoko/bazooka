﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControl : MonoBehaviour
{
    // ゲームオブジェクトのクマ
    public GameObject bear;
    // スコア
    int score = 0;
    // タイマー
    float elapsedTime;
    // ゲーム終了ボイスのフラグ
    bool playedEndVoice = false;
    const float BASE_WIDTH = 511;
    public GUIStyle style = new GUIStyle();
    public GUIStyle endMsgStyle = new GUIStyle();

    // 以下サイズ再調整する
    // スコアメッセージ表示位置
    Rect scoreRect = new Rect(400, 10, 70, 30);
    // タイマー表示位置
    Rect timerRect = new Rect(350, 50, 120, 30);
    // 終了メッセージ表示位置
    Rect endMsgRect = new Rect(50, 10, 200, 35);
    // 
    Rect rankMsgRect = new Rect(50, 85, 250, 30);
    // リプレイボタン表示位置
    Rect replayBtnRect = new Rect(50, 150, 120, 40);

    void Start()
    {
        // クマのオブジェクトをを9体生成
        for (int i = 0; i < 9; i++)
        {
            Instantiate(bear);
        }

        // スコアメッセージ
        scoreRect = MakeRect(scoreRect);
        // タイマー
        timerRect = MakeRect(timerRect);
        // 終了メッセージ
        endMsgRect = MakeRect(endMsgRect);
        // 
        rankMsgRect = MakeRect(rankMsgRect);
        //replayBtnRect = MakeRect(replayBtnRect);
    }

    void Update()
    {
        // ゲームが継続しているとき
        if (!FinishedGame())
        {
            // 経過時間を加算
            elapsedTime += Time.deltaTime;
        }
    }

    public void AddScore()
    {
        // スコアの加算
        score++;
    }

    // ゲームの終了判定
    bool FinishedGame()
    {
        // クマがいなくなったら
        return GameObject.FindWithTag("Teddy") == null;
    }

    float GetValueByScreenSize(float x)
    {
        float ratio = Screen.width / BASE_WIDTH;
        return x * ratio;
    }

    Rect MakeRect(Rect defaultRect)
    {
        float left = GetValueByScreenSize(defaultRect.left);
        float top = GetValueByScreenSize(defaultRect.y);
        float width = GetValueByScreenSize(defaultRect.width);
        float height = GetValueByScreenSize(defaultRect.height);
        Rect newRect = new Rect(left, top, width, height);
        return newRect;
    }

    void OnGUI()
    {
        // スコアの表示
        GUI.Label(scoreRect, "スコア " + score, style);
        // タイマーの表示
        GUI.Label(timerRect, "タイム " + elapsedTime, style);

        // ゲームが終了したとき
        if (FinishedGame())
        {
            // 終了時セリフフラグがfalseの時
            if (playedEndVoice == false)
            {
                // 終了ボイスを呼び出す
                SoundManager.Instance.PlayEndVoice();
                playedEndVoice = true;
            }

            // 終了時に表示するメッセージ
            GUI.Label(endMsgRect, "Game Clear!!", endMsgStyle);
            GUI.Label(rankMsgRect, "あなたのタイムは" + elapsedTime + "です！", style);
            // リプレイボタンが押下されたとき
            if (GUI.Button(replayBtnRect, "もう一度プレイする"))
            {
                // ゲームを最初から開始
                Application.LoadLevel("Teddy Bear Bazooka");
            }
        }
    }
}
